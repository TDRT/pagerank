package pr.com;

import java.io.IOException;
import java.util.ArrayList;

public class MultiThreadStart {

	public static double[] pageRankVector = new double[100];
	public static int threadCount = 4;
	public static int threadCountV2 = 50;
	double probMatrix[][];
	double prVector[];
	long time = 0;

	PageRankDM prdm = new PageRankDM();
	Parser parse = new Parser();

	public MultiThreadStart(double[][] probMatrix) {

		this.probMatrix = probMatrix;

	}

	public void init(double[] vector) {

		time = System.currentTimeMillis();
		for (int i = 0; i < 4; i++) {
			MultiThread thread = new MultiThread(probMatrix, vector, i * 25,
					this);
			Thread newThread = new Thread(thread);
			newThread.start();

		}

	}

	public synchronized void countThread() throws IOException {
		threadCount--;
		if (threadCount == 0) {
			threadCount = 4;
			threadCountV2--;

			if (threadCountV2 > 0) {
				prVector = pageRankVector.clone();
				init(prVector);
			} else {

				ArrayList<String> list = parse.linkListInput();
				int tempV[] = new int[prVector.length];
				for (int i = 0; i < tempV.length; i++) {
					tempV[i] = i;
				}

				prdm.vectorSort(prVector, tempV);
				prdm.matrixSort(prVector);

				int count = 1;
				for (int i = prVector.length - 1; i >= 0; i--) {
					System.out.println(count + ":" + list.get(tempV[i])
							+ "====" + prVector[i]);
					count++;
				}

				System.out.println();
				System.out.println(System.currentTimeMillis() - time);

			}

		}
	}

}
