package pr.com;

import java.util.ArrayList;

public class PageRank {

	public double[] pageRank(ArrayList<String> list, int mass[][],
			double array[]) {

		double d = 0.85; // ����������� ���������
		double array2[] = new double[list.size()];

		// ������ ������
		for (int i = 0; i < array.length; i++) {

			double sum = 0;

			// ������ ������� �� ��������� (1 �������� �� 1 ��������)
			for (int j = 0; j < mass.length; j++) {

				// ���� �������� ��������� �� i �������
				if (mass[j][i] == 1) {

					int linkCount = 0; // ���������� ��������� ������ ��������

					// ������ ���������� ��������� ������
					for (int k = 0; k < mass.length; k++) {
						if (mass[j][k] == 1) {
							linkCount++;
						}
					}
					// ������ ����� ����� (PR(i)/linkCount(i))
					sum = sum + (array[j] / linkCount);
				}

			}

			array2[i] = (1 - d) + d * sum;

		}

		for (int i = 0; i < array2.length; i++) {
			array[i] = array2[i];
		}

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}

		return array;

	}

	// ���������� ������� PageRank
	public double[] arrayFill(int mass[][]) {

		double array[] = new double[mass.length];

		for (int i = 0; i < array.length; i++) {
			array[i] = (double) 1 / array.length;
		}

		return array;
	}

	// ����������� ������� PageRank
	public void arrayShow(double[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}

	// ���������������� �������
	public double[][] transposeMatrix(double[][] probMatrix) {

		double probMatrix2[][] = new double[probMatrix.length][probMatrix.length];

		for (int i = 0; i < probMatrix.length; i++) {
			for (int j = 0; j < probMatrix.length; j++) {
				probMatrix2[i][j] = probMatrix[j][i];
			}
		}

		return probMatrix2;
	}

}
