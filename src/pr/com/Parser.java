package pr.com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Parser {

	public static Elements parse(String link) throws IOException {
		Document doc = Jsoup.connect(link).get();
		Elements links = doc.select("a");
		return links;
	}

	// ���������� ������ �� 100 ������
	public ArrayList<String> linkList() throws IOException {

		ArrayList<String> list = new ArrayList<String>();
		for (Element link : parse("http://www.world-art.ru/animation/")) {
			String absHref = link.attr("abs:href"); // "http://jsoup.org/"
			if (list.size() < 100 & !list.contains(absHref)
					& (absHref.equals("")) & (absHref.contains("world-art"))) {
				list.add(absHref);
			}
		}

		return list;
	}

	// ���������� ������� ���������
	public int[][] adjacencyMatrix(ArrayList<String> list) throws IOException {

		int mass[][] = new int[100][100];
		for (int r = 0; r < list.size(); r++) {
			for (Element link2 : parse(list.get(r))) {
				String absHref = link2.attr("abs:href"); // "http://jsoup.org/"
				if (list.size() < 100 & !list.contains(absHref)) {
					list.add(absHref);
				}
				if (list.contains(absHref)) {
					mass[r][list.indexOf(absHref)] = 1;
				}
			}
		}

		return mass;

	}

	// ����������� �������
	public void matrixShow(int[][] mass) {

		int count = 1;
		for (int i = 0; i < mass.length; i++) {
			System.out.print(count + ":" + " ");
			count++;
			for (int j = 0; j < mass.length; j++) {
				System.out.print(mass[i][j] + " ");
			}
			System.out.println();
		}

	}

	// ������ ������ ������ � ����
	public void writeLinkList(ArrayList<String> list) {

		File file = new File("C://JavaLesson//PageRank//linkList.txt");

		try (FileWriter writer = new FileWriter(file, false)) {

			for (String item : list) {
				writer.write(item);
				writer.write(System.lineSeparator());

			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	// ������ ������� � ����
	public void writeAdjancencyMatrix(int mass[][]) {

		File file = new File("C://JavaLesson//PageRank//matrix.txt");

		try (FileWriter writer = new FileWriter(file, false)) {

			for (int i = 0; i < mass.length; i++) {
				for (int j = 0; j < mass.length; j++) {
					String s = Integer.toString(mass[i][j]);
					s += " ";
					writer.write(s);
				}
				writer.write(System.lineSeparator());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	// ������ ������ ������ �� �����
	public ArrayList<String> linkListInput() throws IOException {

		File file = new File("C://JavaLesson//PageRank//linkList.txt");
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<String> list = new ArrayList<String>();
		try {
			String temp;

			while ((temp = reader.readLine()) != null) {
				list.add(temp);
			}
		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}

		reader.close();

		return list;
	}

	// ������ ������� �� �����
	public int[][] adjacencyMatrixInput() throws FileNotFoundException {

		Scanner in = new Scanner(new File(
				"C://JavaLesson//PageRank//matrix.txt"));

		int[] tempIntegerArray = new int[10000];
		int p = 0;
		while (in.hasNextInt()) {
			tempIntegerArray[p++] = in.nextInt();
		}

		in.close();

		p = 0;
		int mass2[][] = new int[100][100];

		for (int i = 0; i < mass2.length; i++) {
			for (int j = 0; j < mass2.length; j++) {
				mass2[i][j] = tempIntegerArray[p];
				p++;
			}
		}

		return mass2;
	}

}
