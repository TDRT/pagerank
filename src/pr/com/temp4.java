package pr.com;

import java.io.IOException;
import java.util.ArrayList;

public class temp4 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Parser parse = new Parser();
		CompressedRowStorage crs = new CompressedRowStorage();
		PageRank pr = new PageRank();

		int mass[][] = parse.adjacencyMatrixInput();
		ArrayList<String> list = parse.linkListInput();

		ArrayList<Integer> columnArr = crs.columnArrReturn(mass);
		ArrayList<Integer> valueArr = crs.valueArrReturn(mass);
		ArrayList<Integer> lineArr = crs.lineArrReturn(mass);
		double[] array = pr.arrayFill(mass);

		for (int i = 0; i < 200; i++) {
			crs.pageRank(columnArr, valueArr, lineArr, list, array, mass);
			System.out.println();
		}

	}

}
