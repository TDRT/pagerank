package pr.com;

import java.io.IOException;

public class MultiThread implements Runnable {

	double prVector[];
	double probMatrix[][];
	int bPos;
	MultiThreadStart mts;

	public MultiThread(double[][] probMatrix, double[] prVector, int bPos,
			MultiThreadStart anyV) {

		this.probMatrix = probMatrix;
		this.prVector = prVector;
		this.bPos = bPos;
		this.mts = anyV;

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		for (int i = bPos; i < bPos + 25; i++) {
			double temp = 0;
			for (int j = 0; j < probMatrix.length; j++) {

				temp = temp + prVector[j] * probMatrix[j][i];
			}

			MultiThreadStart.pageRankVector[i] = temp;
		}

		try {
			mts.countThread();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
