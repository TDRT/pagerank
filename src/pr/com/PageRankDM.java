package pr.com;

public class PageRankDM {

	public double[] multiplicationMatrix(double[][] probMatrix,
			double[] prVector) {

		double[] pageRankVector = new double[prVector.length];

		for (int i = 0; i < probMatrix.length; i++) {
			double temp = 0;
			for (int j = 0; j < probMatrix.length; j++) {

				temp = temp + prVector[j] * probMatrix[j][i];
			}

			pageRankVector[i] = temp;
		}

		for (int i = 0; i < pageRankVector.length; i++) {
			System.out.print(pageRankVector[i] + " ");
		}

		for (int i = 0; i < prVector.length; i++) {
			prVector[i] = pageRankVector[i];
		}

		return prVector;

	}

	public double[][] matrixCreate(int mass[][]) {

		double probMatrix[][] = new double[mass.length][mass.length];

		for (int i = 0; i < mass.length; i++) {

			int count = 0;

			for (int j = 0; j < mass.length; j++) {

				if (mass[i][j] == 1) {
					count++;
				}
			}

			if (count == 0) {
				for (int j = 0; j < mass.length; j++) {
					probMatrix[i][j] = (double) 1 / mass.length;

				}
				continue;
			}

			for (int j = 0; j < mass.length; j++) {

				if (mass[i][j] == 1) {
					probMatrix[i][j] = (double) 1 / count;
				}
			}

		}

		for (int i = 0; i < mass.length; i++) {

			for (int j = 0; j < mass.length; j++) {

				System.out.print(probMatrix[i][j] + " ");
			}
			System.out.println();

		}

		return probMatrix;
	}

	public double[] prVectCreate(int mass[][]) {

		double[] prVector = new double[mass.length];

		for (int i = 0; i < mass.length; i++) {
			prVector[i] = (double) 1 / mass.length;
		}

		return prVector;
	}

	public int[] vectorSort(double[] array, int[] tempV) {
		boolean swapped = true;
		int j = 0;
		double tmp;
		int temp2;
		while (swapped) {
			swapped = false;
			j++;
			for (int i = 0; i < array.length - j; i++) {
				if (array[i] > array[i + 1]) {
					tmp = array[i];
					temp2 = tempV[i];
					array[i] = array[i + 1];
					tempV[i] = tempV[i + 1];
					array[i + 1] = tmp;
					tempV[i + 1] = temp2;
					swapped = true;
				}
			}
		}

		return tempV;
	}

	public double[] matrixSort(double[] array) {
		boolean swapped = true;
		int j = 0;
		double tmp;

		while (swapped) {
			swapped = false;
			j++;
			for (int i = 0; i < array.length - j; i++) {
				if (array[i] > array[i + 1]) {
					tmp = array[i];

					array[i] = array[i + 1];

					array[i + 1] = tmp;

					swapped = true;
				}
			}
		}

		return array;
	}

}
