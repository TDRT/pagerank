package pr.com;

import java.util.ArrayList;

public class CompressedRowStorage {

	public ArrayList<Integer> columnArrReturn(int mass[][]) {

		ArrayList<Integer> columnArr = new ArrayList<Integer>();

		for (int i = 0; i < mass.length; i++) {
			for (int j = 0; j < mass.length; j++) {
				if (mass[i][j] != 0) {
					columnArr.add(j);
				}
			}
		}
		return columnArr;
	}

	public ArrayList<Integer> valueArrReturn(int[][] mass) {

		ArrayList<Integer> valueArr = new ArrayList<Integer>();

		for (int i = 0; i < mass.length; i++) {
			for (int j = 0; j < mass.length; j++) {
				if (mass[i][j] != 0) {
					valueArr.add(mass[i][j]);
				}
			}
		}
		return valueArr;
	}

	public ArrayList<Integer> lineArrReturn(int mass[][]) {

		ArrayList<Integer> lineArr = new ArrayList<Integer>();
		int count = 0;

		for (int i = 0; i < mass.length; i++) {
			int sum = 0;
			for (int j = 0; j < mass.length; j++) {
				sum = sum + mass[i][j];
			}
			if (sum == 0) {
				lineArr.add(count);
				continue;
			}

			for (int j = 0; j < mass.length; j++) {
				if (mass[i][j] != 0) {
					lineArr.add(count);
					break;
				}
			}

			count = count + sum;

		}

		lineArr.add(count + 1);

		return lineArr;
	}

	public int getMatrixElem(ArrayList<Integer> columnArr,
			ArrayList<Integer> valueArr, ArrayList<Integer> lineArr, int i,
			int j) {

		int value = 0;
		int n1 = lineArr.get(i);
		int n2 = lineArr.get(i + 1);

		for (int k = n1; k < n2; k++) {
			if (k < columnArr.size()) {
				if (columnArr.get(k) == j) {
					value = valueArr.get(k);
					break;
				}
			}
		}

		return value;
	}

	public double[] pageRank(ArrayList<Integer> columnArr,
			ArrayList<Integer> valueArr, ArrayList<Integer> lineArr,
			ArrayList<String> list, double array[], int[][] mass) {

		double d = 0.85; // ����������� ���������
		double array2[] = new double[mass.length];

		for (int i = 0; i < mass.length; i++) {
			double sum = 0;

			for (int j = 0; j < mass.length; j++) {

				int linkCount = 0;
				if (getMatrixElem(columnArr, valueArr, lineArr, j, i) == 1) {

					for (int k = 0; k < mass.length; k++) {

						if (getMatrixElem(columnArr, valueArr, lineArr, j, k) == 1) {
							linkCount++;

						}
					}

					sum = sum + (array[j] / linkCount);
				}

			}

			array2[i] = (1 - d) + d * sum;
		}

		for (int i = 0; i < array2.length; i++) {
			array[i] = array2[i];
		}

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}

		return array;
	}

}
