package pr.com;

import java.io.IOException;
import java.util.ArrayList;

public class AlgorithmRun {

	public static void main(String args[]) throws IOException {

		Parser parse = new Parser();
		PageRank pr = new PageRank();
		CompressedRowStorage crs = new CompressedRowStorage();
		PageRankDM prdm = new PageRankDM();

		int mass[][] = parse.adjacencyMatrixInput();

		ArrayList<String> list = parse.linkListInput();
		double array[] = pr.arrayFill(mass);

		ArrayList<Integer> columnArr = crs.columnArrReturn(mass);
		ArrayList<Integer> valueArr = crs.valueArrReturn(mass);
		ArrayList<Integer> lineArr = crs.lineArrReturn(mass);

		for (int i = 0; i < 200; i++) {
			array = crs.pageRank(columnArr, valueArr, lineArr, list, array,
					mass);
			System.out.println();
		}

		int tempV[] = new int[array.length];
		for (int i = 0; i < tempV.length; i++) {
			tempV[i] = i;
		}

		tempV = prdm.vectorSort(array, tempV);

		array = prdm.matrixSort(array);

		for (int i = array.length-1; i>=0; i--) {
			System.out.println( list.get(tempV[i])+ "=====" + array[i] + " ");

		}

	}

}
