package pr.com;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;

public class temp {

	public static void main(String[] args) throws IOException {

		File file = new File("C://JavaLesson//PageRank//matrix.txt");

		int[][] array = new int[10][10];
		int count = 1;

		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array.length; j++) {
				array[i][j] = count;
				count++;
			}
		}

		try (FileWriter writer = new FileWriter(file, false)) {

			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < array.length; j++) {
					String s = Integer.toString(array[i][j]);
					s += " ";
					writer.write(s);
				}
				writer.write(System.lineSeparator());
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		Scanner in = new Scanner(new File(
				"C://JavaLesson//PageRank//matrix.txt"));

		int[] integerArray = new int[100];
		int p = 0;
		while (in.hasNextInt()) {
			integerArray[p++] = in.nextInt();
		}

		for (int i = 0; i < integerArray.length; i++) {
			System.out.print(integerArray[i] + " ");
		}

		in.close();

		
		
	}
}
