package pr.com;

import java.io.IOException;


public class temp3 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		PageRankDM prdm = new PageRankDM();
		Parser parse = new Parser();

		int mass[][] = parse.adjacencyMatrixInput();
		double[][] probMatrix = prdm.matrixCreate(mass);
		double[] vector = prdm.prVectCreate(mass);

		MultiThreadStart MTS = new MultiThreadStart(probMatrix);
		MTS.init(vector);
		
		System.out.println();
		System.out.println(System.currentTimeMillis());
		
	}
}
