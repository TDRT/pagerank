package pr.com;

import java.io.IOException;
import java.util.ArrayList;

public class temp2 {

	public static void main(String[] args) throws IOException {

		PageRankDM prdm = new PageRankDM();
		Parser parse = new Parser();

		long time = System.currentTimeMillis();
		int mass[][] = parse.adjacencyMatrixInput();
		ArrayList<String> list = parse.linkListInput();
		
		System.out.println("================");

		double prVector[] = prdm.prVectCreate(mass);
		System.out.println();
		System.out.println("================");
		double probMatrix[][] = prdm.matrixCreate(mass);
		System.out.println("================");
		for (int i = 0; i < 50; i++) {
			prVector = prdm.multiplicationMatrix(probMatrix, prVector);
			System.out.println();
		}
		System.out.println("==========================");
		int tempV[] = new int[prVector.length];

		for (int i = 0; i < tempV.length; i++) {
			tempV[i] = i;
		}

		prdm.vectorSort(prVector, tempV);

		for (int i = 0; i < tempV.length; i++) {
			System.out.print(tempV[i] + " ");
		}
		
		prdm.matrixSort(prVector);
		
		System.out.println();

		for (int i = list.size()-1; i >=0 ; i--) {
			System.out.println(list.get(tempV[i]) + " ===" + prVector[tempV[i]]);
		}
		System.out.println();
		
		System.out.println(System.currentTimeMillis() - time);

	}
}
